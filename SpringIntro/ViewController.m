//
//  ViewController.m
//  SpringIntro
//
//  Created by Michael Childs on 1/12/16.
//  Copyright © 2016 Michael Childs. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
//    UILabel* lbl = [[UILabel alloc]initWithFrame:CGRectMake(100, 100, 100, 50)];
//    
//    lbl.text = @"Hello World!";
//    lbl.textColor = [UIColor whiteColor];
//    lbl.backgroundColor = [UIColor redColor];
//    [self.view addSubview:lbl];
    
    UIScrollView* sv = [UIScrollView new];
    sv.frame = self.view.bounds;
    sv.backgroundColor = [UIColor blackColor];
    [self.view addSubview:sv];
    sv.contentSize = CGSizeMake(sv.frame.size.width * 3, sv.frame.size.height * 3);
    
    
    UIView* view1 = [[UIView alloc]initWithFrame:CGRectMake(/*self.view.frame.size.width*/0, 0, sv.frame.size.width, sv.frame.size.height)];
    view1.backgroundColor = [UIColor whiteColor];
    [sv addSubview:view1];
    
    UIView* view2 = [[UIView alloc]initWithFrame:CGRectMake(self.view.frame.size.width, 0, sv.frame.size.width, sv.frame.size.height)];
    view2.backgroundColor = [UIColor redColor];
    [sv addSubview:view2];
    sv.pagingEnabled = YES;
    
    UIView* view3 = [[UIView alloc]initWithFrame:CGRectMake(self.view.frame.size.width * 2, 0, sv.frame.size.width, sv.frame.size.height)];
    view3.backgroundColor = [UIColor greenColor];
    [sv addSubview:view3];
    
    UIView* view4 = [[UIView alloc]initWithFrame:CGRectMake(0, self.view.frame.size.height, sv.frame.size.width, sv.frame.size.height)];
    view4.backgroundColor = [UIColor yellowColor];
    [sv addSubview:view4];
    
    UIView* view5 = [[UIView alloc]initWithFrame:CGRectMake(self.view.frame.size.width, self.view.frame.size.height, sv.frame.size.width, sv.frame.size.height)];
    view5.backgroundColor = [UIColor purpleColor];
    [sv addSubview:view5];
    
    UIView* view6 = [[UIView alloc]initWithFrame:CGRectMake(self.view.frame.size.width * 2, self.view.frame.size.height, sv.frame.size.width, sv.frame.size.height)];
    view6.backgroundColor = [UIColor lightGrayColor];
    [sv addSubview:view6];
    
    UIView* view7 = [[UIView alloc]initWithFrame:CGRectMake(0, self.view.frame.size.height * 2, sv.frame.size.width, sv.frame.size.height)];
    view7.backgroundColor = [UIColor grayColor];
    [sv addSubview:view7];
    
    UIView* view8 = [[UIView alloc]initWithFrame:CGRectMake(self.view.frame.size.width, self.view.frame.size.height * 2, sv.frame.size.width, sv.frame.size.height)];
    view8.backgroundColor = [UIColor cyanColor];
    [sv addSubview:view8];
    
    UIView* view9 = [[UIView alloc]initWithFrame:CGRectMake(self.view.frame.size.width*2, self.view.frame.size.height*2, sv.frame.size.width, sv.frame.size.height)];
    view9.backgroundColor = [UIColor magentaColor];
    [sv addSubview:view9];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
